function filterBy(array,type) {
    return array.filter(function (value) {
        return typeof value !== type
    });
}

console.log(filterBy(['hello', 'world', 23, '23', null],"string"));